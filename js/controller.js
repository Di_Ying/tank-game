function Controller() {
    this.enironment = {};
    this.computerTanks = {};
    this.playerTanks = {};
    this.gifts = {};
    this.bullets = {};
    this.environmentMap = generateMultiDimensionArray(GAME_BOARD_SIZE, GAME_BOARD_SIZE);
    this.computerTanksMap = generateMultiDimensionArray(GAME_BOARD_SIZE, GAME_BOARD_SIZE);
    this.playerTanksMap = generateMultiDimensionArray(GAME_BOARD_SIZE, GAME_BOARD_SIZE);
    this.giftsMap = generateMultiDimensionArray(GAME_BOARD_SIZE, GAME_BOARD_SIZE);
    this.bulletsMap = generateMultiDimensionArray(GAME_BOARD_SIZE + 1, GAME_BOARD_SIZE + 1);
    this.QUEUE_LENGTH = 100;
    this.queue = [];
    this.nextQueue = [];
    this.bulletCounter = 0;
    this.startLoopIntervalId = null;
}

Controller.prototype.startLoop = function(){
    var self = this;
    this.startLoopIntervalId = setInterval(function(){self.processControllerQueue()},10);
};

Controller.prototype.endLoop = function(){
    clearInterval(this.startLoopIntervalId);
};

Controller.prototype.processControllerQueue = function(){
    /* fetch first 100 command from the next queue and Controller them in this cycle.*/
    var queueLength = Math.min(this.nextQueue.length , this.QUEUE_LENGTH);
    this.queue = this.nextQueue.slice(0,queueLength);
    /* remove the commands that moved to queue */
    this.nextQueue = this.nextQueue.slice(queueLength);
    while(this.queue.length > 0){
        this.execute(this.queue[0]);
        this.queue.shift();
    }
};

Controller.prototype.execute = function(request){

    var target = request.target;
    var command = request.command;
    var left = target.left;
    var top = target.top;
    var direction = request.direction;
    var speed = request.speed;

    if(!target){
        return;
    }

    if(target instanceof Bullet){
        if(command == "add"){
            this.addABullet(target);
        }
        else if(command == "move"){
            this.moveABullet(target); 
        }
        return;
    }

    else if(target instanceof Tank){
        if(command == "add"){
            this.addATank(target);
        }
        else if(
            (!target.IsPlayer() && this.computerTanks[target.getId()])
                || (target.IsPlayer() && this.playerTanks[target.getId()])){
            switch (command){
                //case "add"  : this.addATank(target);break;
                case "move" : this.moveATank(target);break;
                case "fireABullet" : this.fireABullet(target);break;
            }
        }
    }
    return;
};


Controller.prototype.addABullet = function(bullet){
    bullet.id = this.bulletCounter++;
    $("#bulletContainer").append(bullet.createDivElement());
    this.bullets[bullet.getId()] = bullet;
    this.bulletsMap[bullet.y][bullet.x] = bullet.getId();
    var that = this;
    setTimeout(function(){that.moveABullet(bullet)},100);
};

Controller.prototype.removeABullet = function(bullet){
    bullet.removeDivElement();
    this.bulletsMap[bullet.y][bullet.x] = undefined;
    delete this.bullets[bullet.getId()];
};

Controller.prototype.moveABullet = function(bullet){
    //console.log("bullet is moved");
    if(bullet.x <= 0 || bullet.x >= GAME_BOARD_SIZE || bullet.y <= 0 || bullet.y >= GAME_BOARD_SIZE){
        this.removeABullet(bullet);
    }
    else{
        this.bulletsMap[bullet.y][bullet.x] = undefined;
        switch(bullet.direction){
            case "left" : bullet.x--;break;
            case "top"  : bullet.y--;break;
            case "bottom" : bullet.y++; break;
            case "right" : bullet.x++;break;
            default : break;
        }
        this.bulletsMap[bullet.y][bullet.x] = bullet.getId();

        if(bullet.isFiredByPlayer()){
            var targetedArea = bullet.getTargetedArea();
            for(var i = 0 ; i < targetedArea.length; i ++) {
                //targetedArea[i][1] is top ;
                //targetedArea[i][0] is left;
                // map is always y first ,x second
                var computerTankId = this.computerTanksMap[targetedArea[i][1]][targetedArea[i][0]];
                if(computerTankId && computerTankId!=bullet.firedBy.getId()){
                    this.removeABullet(bullet);
                    this.removeATank(this.computerTanks[computerTankId]);
                    bullet.firedBy.score++;
                    $("#score").html(bullet.firedBy.score);
                    //console.log(bullet.firedBy.getId() + "'s score is :" + bullet.firedBy.score);
                    return;
                }
            }
        }
        else{
            var targetedArea = bullet.getTargetedArea();
            for(var i = 0 ; i < targetedArea.length; i ++) {
                //targetedArea[i][1] is top ;
                //targetedArea[i][0] is left;
                // map is always y first ,x second
                var playerTankId = this.playerTanksMap[targetedArea[i][1]][targetedArea[i][0]];
                if(playerTankId && playerTankId != bullet.firedBy.getId()){
                    this.removeABullet(bullet);
                    this.removeATank(this.playerTanks[playerTankId]);
                    return;
                }
            }
        }
        bullet.updateLocation();
        var that = this;
        setTimeout(function(){that.moveABullet(bullet)} , 100);
    }
};

Controller.prototype.isPlaceOccupiedByTank = function(tank){
    var scope = tank.getScope();
    for(var i = 0 ; i < scope.length ; i ++){
        //scope[i][1] is top , scope[i][0] is left
        if( this.playerTanksMap[scope[i][1]][scope[i][0]]){
            if(tank.IsPlayer() && this.playerTanksMap[scope[i][1]][scope[i][0]] == tank.getId()) {
                continue;
            }
            return true;
        }
        if( this.computerTanksMap[scope[i][1]][scope[i][0]]){
            if(!tank.IsPlayer() && this.computerTanksMap[scope[i][1]][scope[i][0]] == tank.getId()) {
                continue;
            }
            return true;
        }
    }
    return false;
};


Controller.prototype.addATank = function(tank){
    if(this.isPlaceOccupiedByTank(tank)){
        var self = this;
        var rand = Math.random() * 10000;
        setTimeout(function(){self.addATank(tank)} , rand);
        return;
    }
    $("#tankContainer").append(tank.createDivElement());
    if(tank.IsPlayer()){
        this.playerTanks[tank.getId()] = tank;
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.playerTanksMap[scope[i][1]][scope[i][0]] = tank.getId();
        }
    }
    else{
        this.computerTanks[tank.getId()] = tank;
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.computerTanksMap[scope[i][1]][scope[i][0]] = tank.getId();
        }
    }
    return true;
};

Controller.prototype.removeATank = function(tank){
    if(tank.IsPlayer()){
        //scope contains bricks occupied by the tank
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.playerTanksMap[scope[i][1]][scope[i][0]] = undefined;
        }
        delete this.playerTanks[tank.getId()];
    }
    else{
        //scope contains bricks occupied by the tank
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.computerTanksMap[scope[i][1]][scope[i][0]] = undefined;
        }
        delete this.computerTanks[tank.getId()];
    }
    tank.removeDivElement();
};

Controller.prototype.moveATank = function(tank){

    var newTank = $.extend(true, {}, tank);
    var direction = tank.direction;

    if(tank.IsPlayer()) {
        tank = this.playerTanks[tank.getId()];
    }
    else{
        tank = this.computerTanks[tank.getId()];
    }
    tank.direction = direction;

    switch(newTank.direction){
        case "left" : if(newTank.x > 0)newTank.x--;break;
        case "top"  : if(newTank.y > 0)newTank.y--;break;
        case "bottom" : if(newTank.y < GAME_BOARD_SIZE - 2)newTank.y++; break;
        /* GAME_BOARD_SIZE is 26 , tank is occupying 2 bricks , so last position is from 24 to 26
         24 is the last start point , if it equals 24 then there is no way to move further
         */
        case "right" : if(newTank.x < GAME_BOARD_SIZE - 2)newTank.x++; break;
        default : break;
    }

    // if any brick is occupied by any object, then stop tank from moving to that brick
    // TODO: add env check later
    if(this.isPlaceOccupiedByTank(newTank)){
        tank.updateDivElement();
        return;
    }
    /*
    var scope = newTank.getScope();
    
    for(var i = 0 ; i < scope.length ; i ++){
        //scope[i][1] is top , scope[i][0] is left
        if( this.playerTanksMap[scope[i][1]][scope[i][0]]){
            if(tank.IsPlayer() && this.playerTanksMap[scope[i][1]][scope[i][0]] == tank.getId()) {
                continue;
            }
            tank.updateDivElement();
            return;
        }
        if( this.computerTanksMap[scope[i][1]][scope[i][0]]){
            if(!tank.IsPlayer() && this.computerTanksMap[scope[i][1]][scope[i][0]] == tank.getId()) {
                continue;
            }
            tank.updateDivElement();
            return;
        }
    }*/

    if(tank.IsPlayer()){
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.playerTanksMap[scope[i][1]][scope[i][0]] = undefined;
        }
    }
    else{
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.computerTanksMap[scope[i][1]][scope[i][0]] = undefined;
        }
    }

    switch(tank.direction){
        case "left" : if(tank.x > 0)tank.x--;break;
        case "top"  : if(tank.y > 0)tank.y--;break;
        case "bottom" : if(tank.y < GAME_BOARD_SIZE - 2)tank.y++; break;
        /* GAME_BOARD_SIZE is 26 , tank is occupying 2 bricks , so last position is from 24 to 26
         24 is the last start point , if it equals 24 then there is no way to move further
         */
        case "right" : if(tank.x < GAME_BOARD_SIZE - 2)tank.x++; break;
        default : break;
    }

    if(tank.IsPlayer()){
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.playerTanksMap[scope[i][1]][scope[i][0]] = tank.getId();
        }
    }
    else{
        var scope = tank.getScope();
        for(var i = 0 ; i < scope.length ; i ++){
            //scope[i][1] is top , scope[i][0] is left
            this.computerTanksMap[scope[i][1]][scope[i][0]] = tank.getId();
        }
    }

    var targetedArea = tank.getTargetedArea();
    var tankIsSunk = false;
    for(var i = 0 ; i < targetedArea.length; i ++) {
        //targetedArea[i][1] is top ;
        //targetedArea[i][0] is left;
        // map is always y first ,x second
        var bulletId = this.bulletsMap[targetedArea[i][1]][targetedArea[i][0]];
        if (bulletId && this.bullets[bulletId]) {
            var bullet = this.bullets[bulletId];
            if (bullet.isFiredByPlayer() ^ tank.IsPlayer()) {
                tankIsSunk = true;
                this.removeABullet(bullet);
                this.removeATank(tank);
                bullet.firedBy.score++;
                $("#score").html(bullet.firedBy.score);
                //console.log(bullet.firedBy.getId() + "'s score is :" + bullet.firedBy.score);
                break;
            }
        }
    }
    if(!tankIsSunk){
        tank.updateDivElement();
    }
};

Controller.prototype.fireABullet = function(tank){
    var bulletPosition = tank.getBulletPosition();
    var bullet = new Bullet("" , bulletPosition[0] , bulletPosition[1] , tank.direction , tank);
    this.addABullet(bullet);
};




Controller.prototype.addAPlayer = function(){

};

Controller.prototype.removeAPlayer = function(){

};

Controller.prototype.addAGift = function(){

};


Controller.prototype.removeAGift = function(){

};

Controller.prototype.addAEnviron = function(){

};

Controller.prototype.removeAEnviron = function(){

};



function Request(target , command , direction ,left, top , speed){
    this.target = target;
    this.command = command;
    this.direction = direction;
    this.left = left;
    this.top = top;
    this.speed = speed;
}