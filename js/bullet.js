function Bullet(icon , x , y , direction , tank){
    this.name = "bullet";
    this.type = "normal";
    this.icon = icon;
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.firedBy = tank;
    this.id = -1;
    this.className = "Bullet";

}

Bullet.prototype.getId = function(){
    return this.firedBy.getId() + "-" + this.id;
};

Bullet.prototype.createDivElement = function(){

    var div = document.createElement("div");
    div.id = this.getId();
    var $div = $(div);
    $div.addClass("bullet");
    $div.css({
        left :  this.x * GAME_BOARD_BRICK_SIZE - 10,
        top  :  this.y * GAME_BOARD_BRICK_SIZE - 10
    });
    return $div;

};

Bullet.prototype.removeDivElement = function(){
    $("#" + this.getId()).remove();
};

Bullet.prototype.updateLocation = function(x , y ){
    if(x){
        this.x = x ;
    }
    if(y){
        this.y = y ;
    }
    $("#" + this.getId()).css({
        left :  this.x * GAME_BOARD_BRICK_SIZE - 10,
        top  :  this.y * GAME_BOARD_BRICK_SIZE - 10
    })
};

Bullet.prototype.isFiredByPlayer = function(){
    return this.firedBy.IsPlayer();
};

Bullet.prototype.getTargetedArea = function(){
    var targetedArea = [];
    switch(this.direction){
        case "left" : targetedArea =  [[this.x -1,this.y-1],[this.x -1,this.y]];break;
        case "top"  : targetedArea =  [[this.x -1,this.y-1],[this.x ,this.y-1]];break;
        case "right": targetedArea =  [[this.x   ,this.y-1],[this.x ,  this.y]];break;
        case "bottom" : targetedArea =  [[this.x -1,this.y  ],[this.x ,  this.y]];break;
        default : targetedArea = [];break;
    }

    for(var i = 0 ; i < targetedArea.length;){
        if(targetedArea[i][0] < 0 || targetedArea[i][0] >= GAME_BOARD_SIZE ||
            targetedArea[i][1] < 0 || targetedArea[i][1] >= GAME_BOARD_SIZE){
            targetedArea.splice(i,1);
        }
        else{
            i++;
        }
    }
    return targetedArea;
};
/*

function fireABullet(fireLeft , fireTop , fireDirection , tankId){

    var div = document.createElement("div");
    div.id = tankId + "-" + "bullet";
    var $div = $(div);
    $div.addClass("bullet");
    $div.css({
        left :  fireLeft,
        top  :  fireTop
    });
    $("#bulletContainer").appendChild($div);
}*/
