function generateMultiDimensionArray(){
    var res = undefined;
    for(var i = arguments.length - 1; i >= 0 ; i --){
        var temp = [];
        var dimensionSize = arguments[i];
        for(var j = 0 ; j < dimensionSize ; j ++){
            if(res == undefined){
                temp.push(res);
            }
            else{
                temp.push(res.slice());
            }
        }
        res = temp;
    }
    return res;
}


function generateKey(x , y){
    return y + "-" + x;
}