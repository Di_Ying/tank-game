function Tank(isPlayer, left, top , type , direction , id , group){
    this.type = "tank";
    this.isPlayer = isPlayer;
    this.x = left;
    this.y = top;
    this.type = type;
    this.direction = direction;
    this.id = id;
    if(group == undefined){
        group = 1;
    }
    this.group = group;
    this.score = 0;
    this.className = "Tank";

    this.generateKey = function(){
        return generateKey(top , left);
    }
}

Tank.prototype.IsPlayer = function(){
    return this.isPlayer;
};

Tank.prototype.getId = function(){
    if(this.IsPlayer()) {
        return "player-" + this.id;
    }
    else{
        return "computer-" + this.id;
    }
};

Tank.prototype.getScope = function(){
    return [[this.x,this.y],[this.x + 1,this.y],[this.x,this.y + 1],[this.x + 1,this.y + 1]];
};

Tank.prototype.getTargetedArea = function(){
    var res = [];
    for(var i = this.x ; i < this.x + 2 ; i++){
        for(var j = this.y ; j < this.y + 2; j++){
            res.push([i , j]);
        }
    } 
    return res;
};


Tank.prototype.createDivElement = function(){
    var div = document.createElement("div");
    div.id = this.getId();
    var $div = $(div);
    var divClass = "tank tankTop";
    switch(this.direction){
        case "left" : divClass = "tank tankLeft";break;
        case "top"  : divClass = "tank tankUp";break;
        case "right" : divClass = "tank tankRight";break;
        case "bottom" : divClass = "tank tankDown";break;
        // if direction is not in the list , then consider it as top.
        default : this.direction = "top" ; divClass = "tank tankUp";
    }
    $div.addClass(divClass);
    $div.css({
        left :  this.x * GAME_BOARD_BRICK_SIZE,
        top  :  this.y * GAME_BOARD_BRICK_SIZE
    });
    return $div;
};

Tank.prototype.updateDivElement = function(){
    var $div = $("#" + this.getId());
    var divClass = "tank tankTop";
    switch(this.direction){
        case "left" : divClass = "tank tankLeft";break;
        case "top"  : divClass = "tank tankUp";break;
        case "right" : divClass = "tank tankRight";break;
        case "bottom" : divClass = "tank tankDown";break;
        // if direction is not in the list , then consider it as top.
        default : this.direction = "top" ; divClass = "tank tankUp";
    }
    $div.removeClass("tankLeft");
    $div.removeClass("tankUp");
    $div.removeClass("tankRight");
    $div.removeClass("tankDown");
    $div.removeClass("tank");
    $div.addClass(divClass);
    $div.css({
        left :  this.x * GAME_BOARD_BRICK_SIZE,
        top  :  this.y * GAME_BOARD_BRICK_SIZE
    });
    return $div;
};

Tank.prototype.removeDivElement = function(){
    $("#" + this.getId()).remove();
};

Tank.prototype.getBulletPosition = function(){
    switch(this.direction){
        case "left"     :   return [this.x , this.y + 1];
        /*  1 2 3         
            4 5 6       this.x means column 2 5 8 or 1 4 7
            7 8 9 
         */
        case "top"      :   return [this.x + 1, this.y ];
        case "right"    :   return [this.x + 2, this.y + 1];
        case "bottom"   :   return [this.x + 1, this.y + 2];
        default : return [];
    }
};